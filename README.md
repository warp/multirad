# multirad - Multi-radical lookup for Japanese kanji

## Acknowledgments

This package uses the
[JMdict/EDICT](http://www.edrdg.org/wiki/index.php/JMdict-EDICT_Dictionary_Project)
and [KANJIDIC](http://www.edrdg.org/wiki/index.php/KANJIDIC_Project) dictionary
files. These files are the property of the Electronic Dictionary Research and
Development Group, and are used in conformance with the Group's licence.

## Installation

1. Download Japanese dictionary files: **KANJIDIC**, **RADKFILE** and
   **KRADFILE**. KANJIDIC is available from most linux package managers.
   RADKFILE and KRADFILE are distributed by Jim Breen at
   http://nihongo.monash.edu/kradinf.html.

2. Save multirad.el to your Emacs load path and `require` it.

3. Customize `multirad-*-path` variables to point to the appropriate paths.

Example init.el snippet:

``` emacs-lisp
(require 'multirad)
(setq multirad-kanjidic-path "path-to-kanjidic")
(setq multirad-radkfile-path "path-to-radkfile")
(setq multirad-kradfile-path "path-to-kradfile")
```

## Usage

Initialize multirad with `M-x multirad`. This loads radical and kanji data and
opens the radical and kanji buffers. The radical buffer displays all radicals
from RADKFILE, by stroke count; the kanji buffer displays the character and
radicals for each kanji in KRADFILE containing all selected radicals.

Select or deselect radicals in either buffer with `SPC`/`<mouse-1>`, or clear
selection with `DEL`. When selecting radicals associated with a kanji in the
kanji buffer, the kanji becomes active and will stay in focus as the list is
updated.

The kanji buffer includes additional dictionary information for each kanji
(currently, stroke count, readings and meanings) as long as
`multirad-kanjidic-p` is enabled, toggled interactively by
`multirad-toggle-kanjidic`. On first activation, data is loaded from KANJIDIC,
which may take some time.

## Encodings and alternate glyphs

The set of radicals used by RADKFILE/KRADFILE differ from the Kangxi radicals
which were incorporated into Unicode. As such, not every radical can be encoded
as a character. Other radicals can be encoded as characters not present in the
original JIS X 0208 charset on which the files were based. In such cases, the
character for a radical is represened by a kanji which contains it, and given an
extra field in RADKFILE: either an alternate EUC-JP character, in cases where
the radical was introduced in JIS X 0212, or the name of an image file on the
WWWJDIC server, to serve as a glyph.

Multirad uses the `display` text property to display radicals with alternate
characters, leaving the underlying buffer contents as the primary encodings from
RADKFILE/KRADFILE.

Downloading glyph images from WWWJDIC is not currently supported. These
radicals will appear in `multirad-alt-glyph-face` and have their `help-echo`
property set to the file name.

## Customization

See Customize group `multirad`.

- Dictionary information from KANJIDIC is loaded automatically by `multirad`
  when `multirad-auto-load-kanjidic` is enabled. Also enable display of
  dictionary information in the kanji buffer when set to `display`. Setting this
  to a non-nil value may increase start up time.

- No kanji will be shown until the filtered list numbers less than
  `multirad-max-kanji`, for performance reasons (5000 by default). Set this to a
  large value to show all kanji when no radicals are selected.

- Radicals in the radical buffer are line-wrapped according to
  `multirad-fill-column`.

- Special marked readings from KANJIDIC are given distinct faces according to
  `multirad-special-reading-faces`.

- A number of custom faces determine character display.
