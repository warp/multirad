;;; multirad.el --- Multi-radical lookup for Japanese kanji -*- lexical-binding: t -*-

;; Author: Walter Lewis
;; URL: https://codeberg.org/warp/multirad
;; Keywords: japanese

;; This file is part of multirad.
;;
;; multirad is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; multirad is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License along with
;; multirad. If not, see <http://www.gnu.org/licenses/>.
;;; Code:

(defcustom multirad-kanjidic-path "/usr/share/edict/kanjidic"
  "Path to the KANJIDIC kanji dictionary file."
  :type '(choice (const "/usr/share/edict/kanjidic") string)
  :group 'multirad)
(defcustom multirad-radkfile-path "/usr/share/edict/radkfile"
  "Path to the RADKFILE radical->kanji dictionary file."
  :type '(choice (const "/usr/share/edict/radkfile") string)
  :group 'multirad)
(defcustom multirad-kradfile-path "/usr/share/edict/kradfile"
  "Path to the KRADFILE kanji->radical dictionary file."
  :type '(choice (const "/usr/share/edict/kradfile") string)
  :group 'multirad)
(defcustom multirad-auto-load-kanjidic nil
  "Whether `multirad' loads KANJIDIC data automatically.
Possible values are nil, t or `display'. Also enable display of
dictionary information in the kanji buffer when set to `display'.
Setting this to a non-nil value may increase start up time."
  :type '(choice (const :tag "off" nil)
                 (const :tag "on" t)
                 (const :tag "display" display))
  :group 'multirad)
(defcustom multirad-auto-enable-kanjidic nil
  "Whether `multirad' loads KANJIDIC data automatically.
This may increase start up time."
  :type 'boolean
  :group 'multirad)
(defcustom multirad-max-kanji 5000
  "Maximum number of kanji to display when selecting radicals."
  :type 'integer
  :group 'multirad)
(defcustom multirad-fill-column 24
  "Column in radical table at which to line-wrap."
  :type 'integer
  :group 'multirad)
(defcustom multirad-special-reading-faces
  '((1 . multirad-reading-nanori-face)
    (2 . multirad-reading-radical-face))
  "Maps each KANJIDIC special reading to the faces used to display it.
Elements are of the form (MARKNUM . FACE), where MARKNUM is an
integer corresponding to a Tn special reading marker from
KANJIDIC, and FACE is the face used to display the reading."
  :type '(alist :key-type (integer :tag "Marker") :value-type face)
  :group 'multirad)

(defface multirad-selected-face
  '((t :inherit highlight))
  "Face used for selected radicals."
  :group 'multirad)
(defface multirad-active-kanji-face
  '((t :inherit warning))
  "Face used for the active kanji."
  :group 'multirad)
(defface multirad-alt-glyph-face
  '((t :underline t))
  "Face used for radicals with an alternate glyph."
  :group 'multirad)
(defface multirad-reading-nanori-face
  '((t :inherit bold))
  "Face used for nanori (i.e. name) readings in kanji buffer."
  :group 'multirad)
(defface multirad-reading-radical-face
  '((t :inherit underline))
  "Face used for special radical name readings in kanji buffer."
  :group 'multirad)

(defvar multirad-radicals-buffer "*radicals*")
(defvar multirad-kanji-buffer "*kanji*")
(defvar multirad-radicals nil)
(defvar multirad-alt-glyphs nil)
(defvar multirad-kanji nil)
(defvar multirad-kanjidic-p nil)
(defvar multirad-kanjidic-loaded-p nil)
(defvar multirad-selections nil)
(defvar multirad-active-radical nil)
(defvar multirad-active-kanji nil)
(defvar multirad-mode-map (make-sparse-keymap))

(defun multirad ()
  "Load dictionary files and display radicals and kanji buffers."
  (interactive)
  (setq multirad-selections nil
        multirad-active-kanji nil)
  (when (buffer-live-p multirad-radicals-buffer)
    (kill-buffer multirad-radicals-buffer))
  (when (buffer-live-p multirad-kanji-buffer)
    (kill-buffer multirad-kanji-buffer))
  (multirad-load-radicals)
  (multirad-load-kanji)
  (when multirad-auto-load-kanjidic
    (multirad-load-kanjidic))
  (switch-to-buffer multirad-radicals-buffer)
  (multirad-display-radicals)
  (multirad-toggle-kanjidic (eq 'display multirad-auto-load-kanjidic)))

(defun multirad-parse-alt-glyph (str)
  (let ((n (string-to-number str 16)))
    (if (= 0 n) str
      (decode-char 'japanese-jisx0212 n))))

;; TODO use a ring for efficient appends, rather than reverse
(defun multirad-load-radicals ()
  (setq multirad-radicals nil
        multirad-alt-glyphs nil
        rs nil strokes nil ks nil)
  (with-temp-buffer
    (insert-file-contents-literally multirad-radkfile-path)
    (decode-coding-region (point-min) (point-max) 'euc-jp)
    (while (re-search-forward "^\\$ \\(.\\) \\([0-9]+\\)\\(?: \\(.*\\)\\)?$" nil t)
      (let* ((rstr (match-string-no-properties 1))
             (r (when (= 1 (length rstr)) (string-to-char rstr)))
             (ss (string-to-number (match-string-no-properties 2)))
             (ag (match-string-no-properties 3)))
        (if (or (null strokes)
                (= ss strokes))
            (push r rs)
          (push (cons strokes rs) multirad-radicals)
          (setq rs (list r)))
        (when (and ag (> (length ag) 0))
          (push (cons r (multirad-parse-alt-glyph ag)) multirad-alt-glyphs))
        (setq strokes ss))))
  (setq multirad-radicals (reverse (cons (cons strokes rs) multirad-radicals))
        multirad-active-radical (cadar multirad-radicals)))

(defun multirad-parse-kanji (str)
  (list (string-to-char str)
        (seq-remove (lambda (c) (eq ?  c))
          (string-to-list (substring str 4)))))

(defun multirad-load-kanji ()
  (with-temp-buffer
    (insert-file-contents-literally multirad-kradfile-path)
    (decode-coding-region (point-min) (point-max) 'euc-jp)
    (re-search-forward "^[^#]")
    (backward-char)
    (setq multirad-kanji
          (mapcar #'multirad-parse-kanji
            (split-string
             (buffer-substring-no-properties (point) (point-max))
             "\n" t))
          multirad-kanjidic-loaded-p nil)))

(defun multirad-parse-kanjidic-stroke-count ()
  (let ((re " S\\([0-9]+\\)")
        ss)
    (while (re-search-forward re (line-end-position) t)
      (push (string-to-number (match-string-no-properties 1)) ss))
    ss))

(defun multirad-parse-kanjidic-readings ()
  (let ((re "\\(?2: T\\(?3:[0-9]+\\)\\)? -?\\(?1:[[:multibyte:]][[:multibyte:].]*-?\\)")
        rs)
    (while (re-search-forward re (line-end-position) t)
      (push (apply #'propertize
              (match-string-no-properties 1)
              (when (match-string-no-properties 2)
                (let ((sr (assoc
                           (string-to-number (match-string-no-properties 3))
                           multirad-special-reading-faces)))
                  (when sr (list 'face (cdr sr))))))
            rs))
    rs))

(defun multirad-parse-kanjidic-meanings ()
  (let ((re " {\\([^}]+\\)}")
        ms)
    (while (re-search-forward re (line-end-position) t)
      (push (match-string-no-properties 1) ms))
    ms))

(defun multirad-parse-kanjidic (k/rs)
  (when (eobp)
    (error "end of file while parsing KANJIDIC"))
  (let ((c (char-after))
        (k (car k/rs)))
    (unless (eq c k)
      (error "kanji mismatch between KRADFILE and KANJIDIC: %s %s" c k)))
  (let ((kd (list (reverse (multirad-parse-kanjidic-stroke-count))
                  (reverse (multirad-parse-kanjidic-readings))
                  (reverse (multirad-parse-kanjidic-meanings)))))
    (forward-line)
    (append k/rs kd)))

(defun multirad-load-kanjidic ()
  (with-temp-buffer
    (insert-file-contents-literally multirad-kanjidic-path)
    (decode-coding-region (point-min) (point-max) 'euc-jp)
    (forward-line)
    (setq multirad-kanji
          (mapcar #'multirad-parse-kanjidic
            multirad-kanji)))
  (setq multirad-kanjidic-loaded-p t))

(defun multirad-unload-kanjidic ()
  (interactive)
  (setq multirad-kanji
        (mapcar (lambda (k/rs/kd) (seq-take k/rs/kd 2))
          multirad-kanji))
  (setq multirad-kanjidic-loaded-p nil))

(defun multirad-toggle-kanjidic (activep)
  (interactive (list (not multirad-kanjidic-p)))
  (when (and (setq multirad-kanjidic-p activep)
             (not multirad-kanjidic-loaded-p)
             (y-or-n-p "Load dictionary information from KANJIDIC?"))
    (multirad-load-kanjidic))
  (multirad-display-kanji
   (multirad-occur multirad-selections multirad-kanji)))

(defun multirad-radical-properties (r)
  (let* ((sel (memq r multirad-selections))
         (agq (assq r multirad-alt-glyphs))
         (ag (and agq (cdr agq))))
    (append
     (list 'multirad-selection (if sel 'selected 'deselected))
     (list 'face (append (and ag (stringp ag) '(multirad-alt-glyph-face))
                         (and sel '(multirad-selected-face))))
     (when agq
       (if (stringp ag) (list 'help-echo ag)
         (list 'display (string ag)))))))

(defun multirad-propertize-radical (r)
  (apply #'propertize
    (string r)
    (multirad-radical-properties r)))

(defun multirad-format-radicals (ss/rs)
  (concat
   (seq-map (lambda (c) (+ c (- ?０ ?0)))
            (number-to-string (car ss/rs)))
   "　"
   (mapconcat #'multirad-propertize-radical (cdr ss/rs) "")))

(defun multirad-display-radicals ()
  (with-selected-window
      (display-buffer (get-buffer-create multirad-radicals-buffer))
    (let ((inhibit-read-only t))
      (delete-region (point-min) (point-max))
      (insert
       (string-join
        (seq-partition
         (mapconcat #'multirad-format-radicals multirad-radicals "　")
         multirad-fill-column)
        "\n")))
    (search-backward (string multirad-active-radical) nil t)
    (read-only-mode 1)
    (multirad-mode 1)))

(defun multirad-propertize-kanji (k)
  (let ((str (string k)))
    (apply #'propertize
      str
      (when (eq k multirad-active-kanji)
        '(face multirad-active-kanji-face)))))

(defun multirad-format-kanji (k/rs/kd)
  (let ((k (nth 0 k/rs/kd))
        (rs (nth 1 k/rs/kd))
        (kd (and multirad-kanjidic-p (seq-drop k/rs/kd 2))))
    (concat
     (multirad-propertize-kanji k) "　"
     (mapconcat #'multirad-propertize-radical rs "")
     (if (null kd) "\n"
       (format " (%s strokes)\n　　%s\n　　%s\n"
         (mapconcat #'number-to-string (nth 0 kd) ", ")
         (string-join (nth 1 kd) "　")
         (string-join (nth 2 kd) "; "))))))

(defun multirad-display-kanji (kanji)
  (with-selected-window
      (display-buffer (get-buffer-create multirad-kanji-buffer))
    (let* ((inhibit-read-only t)
           (len (length kanji)))
      (delete-region (point-min) (point-max))
      (setq header-line-format
            (list (number-to-string len)
                  (if (null multirad-selections) " kanji"
                    " kanji containing ")
                  (apply #'string multirad-selections)))
      (apply #'insert
        (if (> len multirad-max-kanji)
            (list "(maximum kanji limit reached: "
                   (number-to-string multirad-max-kanji)
                   ")")
          (mapcar #'multirad-format-kanji kanji))))
    (if multirad-active-kanji
        (re-search-backward (string ?^ multirad-active-kanji) nil 'limit)
      (goto-char (point-min)))
    (forward-char)
    (search-forward (string multirad-active-radical) (line-end-position) t)
    (backward-char)
    (read-only-mode 1)
    (multirad-mode 1)))

(defun multirad-select (pos)
  (interactive "d")
  (let ((c (char-after pos))
        (sel (get-text-property pos 'multirad-selection))
        (inhibit-read-only t))
    (when sel
      (setq multirad-active-radical c
            multirad-selections
            (cond ((eq sel 'selected)
                   (seq-remove (lambda (r) (eq r c)) multirad-selections))
                  ((eq sel 'deselected)
                   (cons c multirad-selections))))
      
            (if (eq (current-buffer)
                    (get-buffer multirad-radicals-buffer))
                (setq multirad-active-kanji nil)
              (multirad-display-radicals)
              (setq multirad-active-kanji
                    (char-after (line-beginning-position))))
      (set-text-properties pos (1+ pos) (multirad-radical-properties c))
      (multirad-display-kanji
       (multirad-occur multirad-selections multirad-kanji)))))

(defun multirad-clear ()
  (interactive)
  (setq multirad-selections nil
        multirad-active-kanji nil)
  (multirad-display-kanji
   (multirad-occur nil multirad-kanji))
  (with-selected-window (get-buffer-window multirad-radicals-buffer)
    (let ((pos (point)))
      (multirad-display-radicals)
      (goto-char pos))))

(defun multirad-occur (radicals kanji)
  (interactive (list multirad-selections multirad-kanji))
  (if (null radicals) kanji
    (let ((r (car radicals)))
      (multirad-occur
       (cdr radicals)
       (seq-filter
           (lambda (k/rs/kd)
             (memq r (nth 1 k/rs/kd)))
         kanji)))))

(defun multirad-sort-strokes ()
  (interactive)
  (unless multirad-kanjidic-loaded-p
    (error "KANJIDIC not loaded"))
  (multirad-display-kanji
   (seq-sort
    (lambda (k/rs/kd1 k/rs/kd2)
      (let ((kd1 (seq-drop k/rs/kd1 2))
            (kd2 (seq-drop k/rs/kd2 2)))
        (< (caar kd1) (caar kd2))))
    (multirad-occur multirad-selections multirad-kanji))))

(define-key multirad-mode-map (kbd "SPC") 'multirad-select)
(define-key multirad-mode-map (kbd "<mouse-1>") 'multirad-select)
(define-key multirad-mode-map (kbd "RET") 'multirad-occur)
(define-key multirad-mode-map (kbd "DEL") 'multirad-clear)
(define-key multirad-mode-map (kbd "s") 'multirad-sort-strokes)

(define-minor-mode multirad-mode
  "Multi-radical kanji lookup mode.
Select and deselect radicals with \\[multirad-select], or clear
selection with \\[multirad-clear].

Some radicals are displayed as a kanji which contains it, in
`multirad-alt-glyph-face'. These radicals each have an alternate
encoding string in RADKFILE, which see; hover over the kanji to
see this string."
  nil " multirad" multirad-mode-map)

(provide 'multirad)
;;; multirad.el ends here
